import os, shutil
from PIL import Image

modelPath = r"..\assets\minecraft\models\item\cards"
texturePath = r"..\assets\minecraft\textures\item\cards"
teams = ["blue", "red"]

predicates = []
compactInfos = []

def clearFolder(folder):
    for filename in os.listdir(folder):
        f = os.path.join(folder, filename)
        try:
            if os.path.isfile(f) or os.path.islink(f):
                os.unlink(f)
            elif os.path.isdir(f):
                shutil.rmtree(f)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (f, e))

class Card:
    name = "none"
    unique = False
    arrows = [0, 0, 0, 0]
    abilities = []
    def __init__(self, cardDataStr):
        cardData = cardDataStr.split(' ')
        self.name = cardData[0]
        self.unique = (cardData[1] == "u")
        self.arrows = list(cardData[2])
        self.arrows = list(map(int, self.arrows))
        self.abilities = cardData[3].split(',')
    def generateSprites(self, num):
        global teams, texturePath
        for team in teams:
            sprite = Image.new("RGBA", (48, 48), color=0)
            bg = Image.open(f"{team}_card_unique.png" if self.unique else f"{team}_card.png")
            portrait = Image.open("portraits.png")
            arrow1 = Image.open(f"{team}_arrow1.png")
            arrow2 = Image.open(f"{team}_arrow2.png")
            arrow3 = Image.open(f"{team}_arrow3.png")
            left = 3 + (38 * (num % 12))
            top = 3 + (38 * (num // 12))
            portrait = portrait.crop((left, top, left + 32, top + 32))
            if (team == "red"):
                portrait = portrait.transpose(Image.Transpose.FLIP_LEFT_RIGHT)
            sprite.paste(bg)
            sprite.paste(portrait, (8, 8), portrait.convert('RGBA'))
            for d in range(0, 4):
                if (self.arrows[d] == 3):
                    sprite.paste(arrow3, (0, 0), arrow3.convert('RGBA'))
                elif (self.arrows[d] == 2):
                    sprite.paste(arrow2, (0, 0), arrow2.convert('RGBA'))
                elif (self.arrows[d] == 1):
                    sprite.paste(arrow1, (0, 0), arrow1.convert('RGBA'))
                arrow1 = arrow1.rotate(90, Image.Resampling.NEAREST)
                arrow2 = arrow2.rotate(90, Image.Resampling.NEAREST)
                arrow3 = arrow3.rotate(90, Image.Resampling.NEAREST)
            sprite = sprite.resize((184, 184), Image.Resampling.NEAREST)
            sprite.save(texturePath + f"\\card_{self.name}_{team}.png")
            sprite.close()
            bg.close()
            portrait.close()
            arrow1.close()
            arrow2.close()
            arrow3.close()
        print(f"Generated texture for: {self.name}")
    def generateModels(self):
        global teams, modelPath
        for team in teams:
            fullName = f"card_{self.name}_{team}"
            model = "{ \"parent\": \"item/card\", \"textures\": { \"0\": \"item/cards/" + fullName + "\", \"1\": \"item/card_back_" + team + "\" } }"
            modelFile = open(modelPath + "//" + fullName + ".json", "w")
            modelFile.write(model)
            modelFile.close()
        print(f"Generated model for: {self.name}")
    def addPredicate(self, num):
        global teams, predicates
        for i in range(len(teams)):
            team = teams[i]
            fullName = f"card_{self.name}_{team}"
            predicates.append("{\"predicate\": {\"custom_model_data\":" + str(num*2 + i + 2) + "}, \"model\": \"item/cards/" + fullName + "\"}")
        print(f"Added predicate for: {self.name}")
    def addCompactInfo(self):
        global teams, compactInfos
        compactInfos.append(';'.join([self.name, "u" if self.unique else "c", ','.join((map(str, self.arrows)))]))
        print(f"Added compact info for: {self.name}")

clearFolder(modelPath)
clearFolder(texturePath)

cards = []

cardInfoFile = open("cards_info.txt", "r")
for cardData in cardInfoFile.readlines():
    cards.append(Card(cardData))
cardInfoFile.close()

for i in range(len(cards)):
    card = cards[i]
    card.generateSprites(i)
    card.generateModels()
    card.addPredicate(i)
    card.addCompactInfo()

paperModelFile = open("..\\assets\\minecraft\\models\\item\\paper.json", "w")
paperModelFile.write("{ \"parent\": \"item/generated\", \"textures\": { \"layer0\": \"item/paper\" }, \"overrides\": [" + ','.join(predicates) + "]}")
paperModelFile.close()
print("Created paper model")

compactInfoFile = open("card_compact_infos.txt", "w")
compactInfoFile.write("minecraft:book{PublicBukkitValues:{\"justcreativeplus:filetype\":\"text\",\"justcreativeplus:filehead\":\"{ }\",\"justcreativeplus:filebody\":\"" + '|'.join(compactInfos) + "\"}}")
compactInfoFile.close()
print("Created compact info file")

print("Done!")
